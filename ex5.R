install.packages('Amelia')
library(Amelia)
install.packages('dplyr')
library(dplyr)
library(ggplot2)

adult <- read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv'))
Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
            "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
            "Portugal")
Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
          "Taiwan", "Thailand" ,  "Laos", "India",  "Vietnam"    )


North.america <- c("United-States" ,  "Canada", "Puerto-Rico"  ) 

Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
                             "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )
Other <- c("South")
#Q3
levels(adult$marital)
levels(adult$type_employer)
levels(adult$marital) <- c("Not-married", "Married", "Never-married")
levels(adult$type_employer) <- c("Private", "Gov", "Never-worked", "Self", "Without-pay" )

#Q4

missmap(adult, main = "Missind Data", col = c('yellow','black'))
any(is.na(adult)) 

#Q5
adult.without.index <- select(adult, -X)

#Q6
adult.without.index <- select(adult.without.index, -fnlwgt)
ggplot(adult.without.index, aes(x=adult.without.index$age, fill=adult.without.index$income)) + geom_histogram(alpha=0.2, position="identity", binwidth = 0.5)
ggplot(adult.without.index, aes(x=adult.without.index$education_num, fill=adult.without.index$income)) + geom_histogram(alpha=0.2, position="identity", binwidth = 0.5)


#Q7
adult.without.index.train <- sample_frac(adult.without.index, 0.7)
sid <- as.numeric(rownames(adult.without.index.train))
adult.without.index.test <- adult.without.index[-sid,]

#Q8
log.model <- glm(income ~ . , family = binomial(link = 'logit'), adult.without.index.train)
summary(log.model)

predict.probabilities <- predict(log.model, adult.without.index.test, type = 'response')
predict.values <- ifelse(predict.probabilities > 0.5 ,1,0)

#Q9
misClassError <- mean(predict.values != adult.without.index.test$income)

